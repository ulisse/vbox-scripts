#!/bin/bash
BOX_NAME=centos_autotest
OSTYPE=RedHat
DISK_TYPE=vmdk

CREATE=0

#------------------------------------------------------------------------------
TEMPLATES_DIR="`pwd`/templates"
DISKS_DIR="`pwd`/disks"
BASE_DIR="`pwd`/machines"
BOX_DIR="${BASE_DIR}/${BOX_NAME}"

# check if vm exists
VBoxManage showvminfo "${BOX_NAME}" &> /dev/null
if [[ $? == "0" ]]; then
    VBoxManage unregistervm "${BOX_NAME}"
fi

rm -rf ${BASE_DIR}
mkdir -p ${BASE_DIR}
    
if [[ $CREATE == "1" ]]; then

    MEMORY=512
    VRAM=40
     
    echo "Creating VM..."
    # create vm
    VBoxManage createvm --name "${BOX_NAME}" --ostype ${OSTYPE} --basefolder ${BASE_DIR}
    VBoxManage registervm "${BOX_DIR}/${BOX_NAME}.vbox"

    echo "Setting up network..."
    # network
    VBoxManage modifyvm "${BOX_NAME}" --nictype1 82545EM --nic1 nat --natpf1 "http, tcp, , 80, , 8080"

    echo "Setting up other properties..."
    # all other things
    VBoxManage modifyvm "${BOX_NAME}" --usb on --usbehci on
    VBoxManage modifyvm "${BOX_NAME}" --memory ${MEMORY} --vram ${VRAM} --snapshotfolder /tmp/vbox-ulisse \
        --accelerate3d on --clipboard bidirectional --draganddrop bidirectional --audio pulse \
        --audiocontroller hda --mouse usbtablet
else
    # use template
    echo "Creating VM from template..."
    mkdir -p ${BOX_DIR}
    cp ${TEMPLATES_DIR}/${BOX_NAME}.vbox ${BOX_DIR}
    VBoxManage registervm "${BOX_DIR}/${BOX_NAME}.vbox"
fi

echo "Setting up storage..."
# set storage
VBoxManage storagectl "${BOX_NAME}" --name SATA --add sata --controller IntelAhci
VBoxManage storageattach "${BOX_NAME}" --storagectl SATA --port 0 --device 0 --type dvddrive --medium emptydrive
VBoxManage storageattach "${BOX_NAME}" --storagectl SATA --port 1 --device 0 --type hdd --medium "${DISKS_DIR}/${BOX_NAME}.${DISK_TYPE}"

